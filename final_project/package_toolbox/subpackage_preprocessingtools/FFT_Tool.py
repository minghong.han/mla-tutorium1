import numpy as np
from scipy.fft import fft
import matplotlib.pyplot as plt
from package_toolbox.subpackage_preprocessingtools import Processed_Data as proc


'''
This module contains all functions for computing and plotting the spectrum of the data
    def chunks(lst, n)
        ex. chunks(data_fft_pow_array, 800)
    splits a list in evenly sized chunks of size n

    def Compute_Multiple_FFT_in_Chunks(matlist : list, column, rpm)
        ex. Compute_Multiple_FFT_in_Chunks(List_of_matfiles_1750, 'DE_time', '1750')
    computes the frequency spectrum per revolution for all the input files and sets the labeling in an ascending order

    def lot_Multiple_FFT(data: dict)
        ex. lot_Multiple_FFT('099.mat':'DE_time', '228.mat':'DE_time')
    plots the spectrum for the files and columns that are given as input
'''


def chunks(lst, n):
    for i in range(0, len(lst), n):
        yield lst[i:i + n]

def Compute_Multiple_FFT_in_Chunks(matlist : list, column, rpm):
    
    filelist = matlist
    data_fft_pow_array = []
    label_array = []
    file_label = 0

    for file in filelist: 
        filename = file[-7:]
        file_label = file_label + 1

        if rpm == '1750':
            dataset = proc.get_Dataset(matfile_name=filename)

        elif rpm == '1730':
            dataset = proc.get_Dataset_1730(matfile_name=filename)

        for i in range(len(dataset)):
            sample_data = np.array(dataset[i][column])
            sample_freq = 48000
            freq = (sample_freq/2)*np.linspace(0, 1, int(len(sample_data)/2))

            data_fft = fft(sample_data)
            data_fft_amp = (2/len(sample_data))*np.abs(data_fft[0:np.size(freq)])
            data_fft_pow = data_fft_amp**2
            
            data_fft_pow_array = np.append(data_fft_pow_array, data_fft_pow, axis = 0)
            label_array = np.append(label_array, [file_label], axis = 0)
    
        data_fft_pow_chunks = list(chunks(data_fft_pow_array, 800))
    
    return data_fft_pow_chunks, label_array


def Plot_Multiple_FFT(data: dict):
     f = plt.figure()
     f.set_figwidth(15)
     f.set_figheight(5)
     
     sample_freq = 48000
     
     for key,value in data.items():
         
         dataset = proc.get_Dataset(key)
         dataset = proc.get_all_chunks_in_one_df(dataset)
         sample_data = np.array(dataset[value])
         freq = (sample_freq/2)*np.linspace(0, 1, int(len(sample_data)/2))

         data_fft = fft(sample_data)
         data_fft_amp = (2/len(sample_data))*np.abs(data_fft[0:np.size(freq)])
         data_fft_pow = data_fft_amp**2

         plt.plot(freq, data_fft_pow, alpha=0.5, label = str(key))
     
     plt.rc ('axes', titlesize = 15) 
     plt.title('Spectrum')  
     plt.rc ('xtick', labelsize = 15)
     plt.rc ('ytick', labelsize = 15)
     plt.rc ('legend', fontsize = 50)
     plt.xlabel('Frequency (Hz)')
     plt.ylabel('Spectral Density (|X(k)|^2)')
     plt.legend(fontsize = 'x-large')
     plt.grid(True)
     plt.show()