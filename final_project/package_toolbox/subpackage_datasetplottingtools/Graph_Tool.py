import numpy as np
from scipy.signal import savgol_filter
import matplotlib.pyplot as plt
from package_toolbox.subpackage_preprocessingtools import Processed_Data as proc

"""
This module contains following methods for visualizing the datasets. Use "def get_Dataset( matfile_name ):" 
to get all chunks of a .mat file. The method will return list of all chunks of chosen .mat file.

     ### Show 'FE_Time' AND 'DE_Time' of specific chunk
     - def show_graph_all_columns_of_chunk(one_chunk_as_df):
     - def show_graph_all_column_of_chunk_smooth(one_chunk_as_df):

     Argument data --> Dataframe 

     ex.  all_chunks_from_99 = Processed_Data.get_Dataset( "99.mat" ) 
          first_chunk_of_99  = all_chunks_from_99[0]
          show_graph_all_columns_of_chunk(first_chunk_of_99)
-----------------------------------------------------------------------------------------------------------
     ### Show 'FE_Time' OR 'DE_Time' of specific chunk
     - def show_graph_single_columns_of_chunk(data, columnnames: list):
     - def show_graph_single_columns_of_chunk_smooth(data, columnnames):

     Argument data --> Dataframe

     ex.  all_chunks_from_99 = Processed_Data.get_Dataset( "99.mat" ) 
          first_chunk_of_99  = all_chunks_from_99[0]
          show_graph_single_columns_of_chunk(first_chunk_of_99, ["DE_Time"])
-----------------------------------------------------------------------------------------------------------     
     ### Show chunks of different classes in one graph
     - def show_graph_different_classes(data: dict, chunk : int):
     - def show_graph_different_classes_smooth(data: dict, chunk : int):

     Argument data  --> dictionary {"name of mat file" : "column of matfile"}
     Argument chunk --> integer, Number of chunk to analyse

     ex.  all_chunks_from_99 = Processed_Data.get_Dataset( "99.mat" ) 
          first_chunk_of_99  = all_chunks_from_99[0]
          show_graph_different_classes({"99.mat" : "DE_Time", "218.mat" : "DE_Time" },0)

If you want to analyse a whole .mat file, first use "def get_all_chunks_in_one_df(data):" from Processed_Data module. 
This method will return all chunks of the .mat file in one dataframe. Use this output as argument "data" above.

"""

def show_graph_all_columns_of_chunk(one_chunk_as_df):
     f = plt.figure()
     f.set_figwidth(50)
     f.set_figheight(10)
     values_in_x_axis = np.array(range(one_chunk_as_df.shape[0]))

     for column_in_chunk in one_chunk_as_df:
          plt.plot(values_in_x_axis, one_chunk_as_df[column_in_chunk], label = str(column_in_chunk))

     plt.legend(fontsize = 'x-large')
     plt.grid(True)
     plt.show()


def show_graph_all_column_of_chunk_smooth(one_chunk_as_df):
   dict_y_values = dict()
   f = plt.figure()
   f.set_figwidth(50)
   f.set_figheight(10)
   values_in_x_axis = np.array(range(one_chunk_as_df.shape[0]))

   for column_in_chunk in one_chunk_as_df:  

        Normal_Data_array_y = np.array(one_chunk_as_df[column_in_chunk])
        
        dict_y_values[column_in_chunk] = Normal_Data_array_y

   for key,value in dict_y_values.items():
      yhat = savgol_filter(value, 51, 1)
      plt.plot(values_in_x_axis, yhat, label = str(key))
      
   plt.legend(fontsize = 'x-large')
   plt.grid(True)
   plt.show()



def show_graph_single_columns_of_chunk_smooth(one_chunk_as_df, list_of__column_names):
   list_y_values = dict()
   f = plt.figure()
   f.set_figwidth(10)
   f.set_figheight(2)
   values_in_x_axis = np.array(range(one_chunk_as_df.shape[0]))

   for column_name in list_of__column_names:  

        Normal_Data_array_y = np.array(one_chunk_as_df[column_name])
        
        list_y_values[column_name] = Normal_Data_array_y
   
   for key,value in list_y_values.items():
      yhat = savgol_filter(value, 21, 9)
     
      plt.plot(values_in_x_axis, yhat, label = str(key))
   plt.rc ('axes', titlesize = 15) 
   plt.title('Signal in time domain for bearing with ball fault, 1750 rpm')  
   plt.rc ('xtick', labelsize = 15)
   plt.rc ('ytick', labelsize = 15)
   plt.rc ('legend', fontsize = 50)
   plt.xlabel('x_label')
   plt.ylabel('y_label')
   plt.grid(True)
   plt.show()
   
       
def show_graph_single_columns_of_chunk(one_chunk_as_df, list_of__column_names: list):
     f = plt.figure()
     f.set_figwidth(50)
     f.set_figheight(10)
     values_in_x_axis = np.array(range(one_chunk_as_df.shape[0]))

     for column_name in list_of__column_names:
          plt.plot(values_in_x_axis, one_chunk_as_df[column_name], label = str(column_name))

     plt.legend(fontsize = 'x-large')
     plt.grid(True)
     plt.show()


def show_graph_different_classes(matfile_name_and_column_names_as_dic: dict, chunk : int):
     f = plt.figure()
     f.set_figwidth(50)
     f.set_figheight(10)

     for matfile_name,column_names in matfile_name_and_column_names_as_dic.items():

          dataset = proc.get_Dataset(matfile_name)
          chosen_chunk_of_dataset = dataset[chunk]
          Normal_Data_array_x = np.array(range(chosen_chunk_of_dataset.shape[0]))
          for one_column in column_names:
               plt.plot(Normal_Data_array_x, chosen_chunk_of_dataset[one_column], label = str(one_column))

     plt.legend(fontsize = 'x-large')
     plt.grid(True)
     plt.show()


def show_graph_different_classes_smooth(matfile_name_and_column_names_as_dic: dict, chunk : int):
    

     for matfile_name,column_names in matfile_name_and_column_names_as_dic.items():

          dataset = proc.get_Dataset(matfile_name)
          chosen_chunk_of_dataset = dataset[chunk]
          Normal_Data_array_x = np.array(range(chosen_chunk_of_dataset.shape[0]))
          for one_column in column_names:
               yhat = savgol_filter(chosen_chunk_of_dataset[one_column], 51, 1)
               plt.plot(Normal_Data_array_x, yhat, label = str(one_column))

     plt.legend(fontsize = 'x-large')
     plt.grid(True)
     plt.show()



