import numpy as np


'''
This module contains all functions for adding timestamps to an already loaded dataset
    def Add_Revolution_Timestamps(data)
        ex. def Add_Revolution_Timestamps(Normal_Data)
    adds timestamps per revolution (timestamps are set to zero after each chunk)
    
    def Add_Total_Timestamps(data)
        ex. Add_Total_Timestamps(Normal_Data)
    add timestamps for the total dataset
'''


def Add_Revolution_Timestamps(data):
    sample_freq = 48000
    for i in range(len(data)-1):
        revolution_time_vec = np.arange(0, len(data[i]), 1)/sample_freq
        data[i]['Revolution_Timestamps'] = revolution_time_vec
    return data



def Add_Total_Timestamps(data):
    sample_freq = 48000
    timestamp_lower_limit = 0
    for i in range(len(data)):
        timestamp_upper_limit = timestamp_lower_limit + len(data[i])
        total_time_vec = np.arange(timestamp_lower_limit, timestamp_upper_limit, 1)/sample_freq
        data[i]['Total_Timestamps'] = total_time_vec
        timestamp_lower_limit = timestamp_upper_limit 
    return data
