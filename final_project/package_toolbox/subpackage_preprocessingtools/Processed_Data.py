import os
import scipy.io
import pandas as pd
import numpy as np
from scipy import stats


'''
This module contains all functions for data preprocessing. 
    def get_Dataset(matfile_name)
        ex. get_Dataset('99.mat')
    ###returns list of chunks (first 22 and last 23 of each chunk already deleted) of ONE specific dataset
       Uses split_dataframe() for splitting chunks 

    def get_all_chunks_in_one_df(data)
        ex. get_all_chunks_in_one_df(data = Output of get_Dataset)
    ###concatenates all chunks of a matfile into one Dataframe
    
    def get_filelist_from_48k_and_Normal_Data(rpm:str)
        ex. get_filelist_from_48k_and_Normal_Data('1750')
    ###returns all filepath of fault data and normal data considering argument(1750 0r 1730)
       Use output of this method for argument in get_all_matfiles_as_dict()

    def get_all_matfiles_as_dict(matlist : list)
    ex. List_of_matfiles              = get_filelist_from_48k_and_Normal_Data('1750')    
        Normal_and_48k_data_1750_dict = get_all_matfiles_as_dict(List_of_matfiles)
    ###returns dictionary of ALL datasets containing all chunks; key = matfile name, value = list of chunks of this specific dataset

    
    def get_stat_features_as_df(Data_as_dict: dict,DE_FE_column :str)
        proc.get_stat_features_as_df(Normal_and_48k_data_1750_dict,'DE_time')
        Normal_and_48k_data_1750_dict --> output of get_all_matfiles_as_dict
    ### returns statistical values of all chunks 
    
    def get_all_chunks_in_list_DE(Data_as_dict: dict)
    
    def get_all_chunks_in_list_FE(Data_as_dict: dict)
    
    def split_dataframe(normal_df, chunk_size = 1645)
        no direct use. This method is used by get_Dataset.
    ### splits dataset into chunks. Removes first 22 and last 23 data points of a chunk.



'''

def get_Dataset( matfile_name ):
    filelist = list()
    dataset = dict()
    # Go through folderstructure in mla-tutorium1
    for root, dirs, files in os.walk('.'):
        # Append all matfiles in filelist
        for name in files:
            if name.split('.')[1] == 'mat':
                name = os.path.join(str(root),str(name))
                filelist.append(name)
        
    # Go through filelist and open mat. Tranform given format to more convenient/useable format
    for file in filelist: 
        mat = scipy.io.loadmat(file)

        for i in list(mat.keys()):
            if i.split('_')[-1] != 'time':
                del mat[i]
            # delete all unnecessary columns
            if (i == 'X098_DE_time') or (i == 'X098_FE_time'): 
                del mat[i]
            
            if file.split('/')[-1] == '217.mat':
                if (i == 'X215_DE_time'):
                    del mat[i]
                if (i == 'X215_FE_time'):
                    del mat[i]

            if i[-7:] == 'BA_time':
                del mat[i]
            
        normal_df = pd.DataFrame()
        mat = dict(sorted(mat.items(), key = lambda item : -len(item[1])))

        for key,value in mat.items():
            column_values = mat[key].flatten().tolist()
            normal_df[key] = pd.Series(column_values)
        normal_df.columns = ['DE_time', 'FE_time']
        # use function split_dataframe to split chunks
        chunkslist = split_dataframe(normal_df, chunk_size = 1645)
        chunkslist.pop()
        dataset[file.split('/')[-1]] = chunkslist
            # return just the chunkslist of demanded matfile
    return dataset[matfile_name]


def get_all_chunks_in_one_df(data):
    result = pd.concat(data,ignore_index=True)
    return result


# Just return list with path of filenames. Choose 1750 rpm or 1730 rpm
def get_filelist_from_48k_and_Normal_Data(rpm:str):
    all_mat_datasets = list()
   
    for root, dirs, files in os.walk('48k_fault_data'):
        
        for name in files:
                name = os.path.join(str(root),str(name))
                if name.split('.')[1] == 'mat' and (name.find(rpm) > 0):
                    all_mat_datasets.append(name)
                    
    for root, dirs, files in os.walk('Normal_data'):
        
        for name in files:
                name = os.path.join(str(root),str(name))
                if name.split('.')[1] == 'mat' and (name.find(rpm) > 0 ):
                    all_mat_datasets.append(name)
    
    return all_mat_datasets

# Same as get_Datset. Only difference: This method returns ALL datasets in a dictionary, not only one specific
def get_all_matfiles_as_dict(matlist : list):
    
    dataset = dict()  
    filelist = matlist

    for file in filelist: 
        mat = scipy.io.loadmat(file)

        for i in list(mat.keys()):
            if i.split('_')[-1] != 'time':
                del mat[i]

            if (i == 'X098_DE_time') or (i == 'X098_FE_time'): 
                del mat[i]

            if file.split('/')[-1] == '217.mat':
                if (i == 'X215_DE_time'):
                    del mat[i]
                if (i == 'X215_FE_time'):
                    del mat[i]
    
            if i.split('_')[-7:] == 'BA_time':
                del mat[i]
        normal_df = pd.DataFrame()
        mat = dict(sorted(mat.items(), key = lambda item : -len(item[1])))

        for key,value in mat.items():
            column_values = mat[key].flatten().tolist()
            normal_df[key] = pd.Series(column_values)

        normal_df.columns = ['DE_time', 'FE_time']
        chunkslist = split_dataframe(normal_df, chunk_size = 1645)
        chunkslist.pop()
        dataset[file.split('/')[-1]] = chunkslist
    
    return dataset


def get_stat_features_as_df(Data_as_dict: dict,DE_FE_column :str):
    X = []
    y = []
    label = 0
    for key,dataset in  Data_as_dict.items():
        label += 1
        for chunks in dataset:
            X.append([
            chunks[DE_FE_column].max() , 
            chunks[DE_FE_column].min(),
            np.std(chunks[DE_FE_column]),
            np.var(chunks[DE_FE_column]),
            np.mean(chunks[DE_FE_column]),
            np.median(chunks[DE_FE_column]),
            stats.skew(chunks[DE_FE_column]),
            stats.kurtosis(chunks[DE_FE_column])
            ]) 
            y.append(label +1)
            continue
            
    Statistical_Feature_df = pd.DataFrame(X,columns = ['Max','Min','std','var','mean','median','skew','kurt'])
    y                      = pd.DataFrame(y)
    return y, Statistical_Feature_df

def get_all_chunks_in_list_DE(Data_as_dict: dict):
    list_of_all_chunks = list()
    y = list()
    label = 0
    

    for key, dataset in Data_as_dict.items():
        label += 1
        for chunk in dataset:
            chunk= chunk['DE_time']
            list_of_all_chunks.append(chunk)
            y.append(label)   
    return y,list_of_all_chunks
    

def get_all_chunks_in_list_FE(Data_as_dict: dict):
    list_of_all_chunks = list()
    y = list()
    label = 0
    for key, dataset in Data_as_dict.items():
        label +=1
        for chunk in dataset:
            # if you want FE_Time change it here
            chunk = chunk['FE_time']
            list_of_all_chunks.append(chunk)
            y.append(label)
    return y,list_of_all_chunks



def split_dataframe(normal_df, chunk_size = 1645): 
    chunks = pd.DataFrame()
    chunkslist = list()
  
    #num_chunks = len(normal_df) // chunk_size + 1
    num_chunks = len(normal_df) // chunk_size + (1 if len(normal_df) % chunk_size else 0)
    for i in range(num_chunks):
        chunks = normal_df[i*chunk_size:(i+1)*chunk_size]
        chunks = chunks.iloc[22:-23]
        chunks['DE_time'].fillna(value= np.mean(chunks['DE_time']))
        chunks['FE_time'].fillna(value= np.mean(chunks['FE_time']))
        len(chunks)
        chunkslist.append(chunks)
    return chunkslist


def get_Dataset_1730( matfile_name ):
    filelist = list()
    dataset = dict()

    for root, dirs, files in os.walk('.'):
        
        for name in files:
            if name.split('.')[1] == 'mat':
                name = os.path.join(str(root),str(name))
                filelist.append(name)

    for file in filelist: 
        mat = scipy.io.loadmat(file)

        for i in list(mat.keys()):
            if i.split('_')[-1] != 'time':
                del mat[i]

            if (i == 'X098_DE_time') or (i == 'X098_FE_time'): 
                del mat[i]

            if file.split('/')[-1] == '217.mat':
                if (i == 'X215_DE_time'):
                    del mat[i]
                if (i == 'X215_FE_time'):
                    del mat[i]
    
            if i[-7:] == 'BA_time':
                del mat[i]
            
        normal_df = pd.DataFrame()
        mat = dict(sorted(mat.items(), key = lambda item : -len(item[1])))

        for key,value in mat.items():
            column_values = mat[key].flatten().tolist()
            normal_df[key] = pd.Series(column_values)
        normal_df.columns = ['DE_time', 'FE_time']
        chunkslist = split_dataframe_1730(normal_df, chunk_size = 1626)
        chunkslist.pop()
        dataset[file.split('/')[-1]] = chunkslist
    
    return dataset[matfile_name]

def get_all_matfiles_as_dict_1730(matlist : list):
    
    dataset = dict()  
    filelist = matlist

    for file in filelist: 
        mat = scipy.io.loadmat(file)

        for i in list(mat.keys()):
            if i.split('_')[-1] != 'time':
                del mat[i]

            if (i == 'X098_DE_time') or (i == 'X098_FE_time'): 
                del mat[i]

            if file.split('/')[-1] == '217.mat':
                if (i == 'X215_DE_time'):
                    del mat[i]
                if (i == 'X215_FE_time'):
                    del mat[i]
    
            if i.split('_')[-7:] == 'BA_time':
                del mat[i]
            

        normal_df = pd.DataFrame()
        mat = dict(sorted(mat.items(), key = lambda item : -len(item[1])))

        for key,value in mat.items():
            column_values = mat[key].flatten().tolist()
            normal_df[key] = pd.Series(column_values)

        normal_df.columns = ['DE_time', 'FE_time']
        chunkslist = split_dataframe_1730(normal_df, chunk_size = 1626)
        chunkslist.pop()
        dataset[file.split('/')[-1]] = chunkslist
    
    return dataset
    
def split_dataframe_1730(normal_df, chunk_size): 
    chunks = pd.DataFrame()
    chunkslist = list()
  
    #num_chunks = len(normal_df) // chunk_size + 1
    num_chunks = len(normal_df) // chunk_size + (1 if len(normal_df) % chunk_size else 0)
    for i in range(num_chunks):
        chunks = normal_df[i*chunk_size:(i+1)*chunk_size]
        chunks = chunks.iloc[13:-13]
        chunks['DE_time'].fillna(value= np.mean(chunks['DE_time']))
        chunks['FE_time'].fillna(value= np.mean(chunks['FE_time']))
        len(chunks)
        chunkslist.append(chunks)
    return chunkslist
