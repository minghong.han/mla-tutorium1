import numpy as np
from scipy.signal import savgol_filter
import matplotlib.pyplot as plt
import pandas as pd
from scipy import stats
from package_toolbox.subpackage_preprocessingtools import Processed_Data as proc

"""
This module contains following methods for visualizing different statistical values of different classes:

     For each method, it is possible to choose between smoothed plot or raw plot. For smoothed plot give True as input.
     For each method the arguments of the same structure.
     e.g. dic = {'228.mat':'FE_time','099.mat':'FE_time','215.mat':'FE_time','240.mat':'FE_time'}

     def show_standard_deviation_of_matfiles(dic :dict,smooth: bool)
     def show_max_values_of_matfiles(dic :dict,smooth: bool)
     def show_min_values_of_matfiles(dic :dict,smooth: bool)
     def show_mean_of_matfiles(dic :dict,smooth: bool)
     def show_skewness_of_matfiles(dic :dict,smooth: bool)
     def show_kurtosis_of_matfiles(dic :dict,smooth: bool)
     def show_median_of_matfiles(dic :dict,smooth: bool)
"""


def show_standard_deviation_of_matfiles(dic :dict,smooth: bool):
     f = plt.figure()
     f.set_figwidth(10)
     f.set_figheight(2)
     df =pd.DataFrame()
     std_list = []
     for key,value in dic.items():
          i = 1
          dataset = proc.get_Dataset(key)
          X_values = np.array(range(len(dataset)))
          for chunk in dataset:
               std = np.std(chunk[value])
               std_list.append(std)
          df[key] = pd.Series(std_list)
          std_list =[]

          if smooth:
               y_values = savgol_filter(df[key], 51, 1)
          else:
               y_values = df[key]

          plt.plot(range(len(df)), y_values, label = key)
     
     plt.rc ('axes', titlesize = 15,labelsize = 15) 
     plt.title('Standard deviation of all chunks from different classes')
     plt.legend(fontsize = 'small',loc = 4)
     plt.rc ('xtick', labelsize = 15)
     plt.rc ('ytick', labelsize = 15)
     plt.xlabel('chunks')
     plt.ylabel('std')
     plt.grid(True)
     plt.show()


def show_max_values_of_matfiles(dic :dict,smooth: bool):
     f = plt.figure()
     f.set_figwidth(10)
     f.set_figheight(2)
     df =pd.DataFrame()
     max_list = []
     for key,value in dic.items():

          dataset = proc.get_Dataset(key)
          X_values = np.array(range(len(dataset)))
          for chunk in dataset:
               max = chunk[value].max()
               max_list.append(max)
          df[key] = pd.Series(max_list)
          max_list =[]

          if smooth:
               y_values = savgol_filter(df[key], 51, 1)
          else:
               y_values = df[key]

          plt.plot(range(len(df)), y_values, label = key)
     plt.rc ('axes', titlesize = 15,labelsize = 15)  
     plt.title('Max. values of all chunks from different classes')
     plt.legend(fontsize = 'small',loc = 4)
     plt.rc ('xtick', labelsize = 15)
     plt.rc ('ytick', labelsize = 15)
     plt.xlabel('chunks')
     plt.ylabel('max')
     plt.grid(True)
     plt.show()


def show_min_values_of_matfiles(dic :dict,smooth: bool):
     f = plt.figure()
     f.set_figwidth(10)
     f.set_figheight(2)
     df =pd.DataFrame()
     min_list = []
     for key,value in dic.items():

          dataset = proc.get_Dataset(key)
          X_values = np.array(range(len(dataset)))
          for chunk in dataset:
               min = chunk[value].min()
               min_list.append(min)
          df[key] = pd.Series(min_list)
          min_list =[]

          if smooth:
               y_values = savgol_filter(df[key], 51, 1)
          else:
               y_values = df[key]

          plt.plot(range(len(df)), y_values, label = key)
     plt.rc ('axes', titlesize = 15,labelsize = 15)  
     plt.title('Min. values of all chunks from different classes')
     plt.legend(fontsize = 'small',loc = 4)
     plt.rc ('xtick', labelsize = 15)
     plt.rc ('ytick', labelsize = 15)
     plt.xlabel('chunks')
     plt.ylabel('min')
     plt.grid(True)
     plt.show()

def show_mean_of_matfiles(dic :dict,smooth: bool):
     f = plt.figure()
     f.set_figwidth(10)
     f.set_figheight(2)
     df =pd.DataFrame()
     mean_list = []
     for key,value in dic.items():

          dataset = proc.get_Dataset(key)
          X_values = np.array(range(len(dataset)))
          for chunk in dataset:
               mean = np.mean(chunk[value])
               mean_list.append(mean)
          df[key] = pd.Series(mean_list)
          mean_list =[]

          if smooth:
               y_values = savgol_filter(df[key], 51, 1)
          else:
               y_values = df[key]

          plt.plot(range(len(df)), y_values, label = key)
     plt.rc ('axes', titlesize = 15,labelsize = 15) 
     plt.title('Mean of all chunks from different classes')
     plt.legend(fontsize = 'small',loc = 4)
     plt.rc ('xtick', labelsize = 15)
     plt.rc ('ytick', labelsize = 15)
     plt.xlabel('chunks')
     plt.ylabel('mean')
     plt.grid(True)
     plt.show()

def show_median_of_matfiles(dic :dict,smooth: bool):
     f = plt.figure()
     f.set_figwidth(10)
     f.set_figheight(2)
     df =pd.DataFrame()
     median_list = []
     for key,value in dic.items():

          dataset = proc.get_Dataset(key)
          X_values = np.array(range(len(dataset)))
          for chunk in dataset:
               median = np.median(chunk[value])
               median_list.append(median)
          df[key] = pd.Series(median_list)
          median_list =[]

          if smooth:
               y_values = savgol_filter(df[key], 51, 1)
          else:
               y_values = df[key]

          plt.plot(range(len(df)), y_values, label = key)
     plt.rc ('axes', titlesize = 15,labelsize = 15)  
     plt.title('Median of all chunks from different classes')
     plt.legend(fontsize = 'small',loc = 4)
     plt.rc ('xtick', labelsize = 15)
     plt.rc ('ytick', labelsize = 15)
     plt.xlabel('chunks')
     plt.ylabel('median')
     plt.grid(True)
     plt.show()

def show_kurtosis_of_matfiles(dic :dict,smooth: bool):
     f = plt.figure()
     f.set_figwidth(10)
     f.set_figheight(2)
     df =pd.DataFrame()
     kurtosis_list = []
     for key,value in dic.items():

          dataset = proc.get_Dataset(key)
          X_values = np.array(range(len(dataset)))
          for chunk in dataset:
               kurtosis = stats.kurtosis(chunk[value])
               kurtosis_list.append(kurtosis)
          df[key] = pd.Series(kurtosis_list)
          kurtosis_list =[]

          if smooth:
               y_values = savgol_filter(df[key], 51, 1)
          else:
               y_values = df[key]

          plt.plot(range(len(df)), y_values, label = key)
     plt.rc ('axes', titlesize = 15,labelsize = 15)  
     plt.title('Kurtosis of all chunks from different classes')
     plt.legend(fontsize = 'small',loc = 4)
     plt.rc ('xtick', labelsize = 15)
     plt.rc ('ytick', labelsize = 15)
     plt.xlabel('chunks')
     plt.ylabel('kurt')
     plt.grid(True)
     plt.show()

def show_skewness_of_matfiles(dic :dict,smooth: bool):
     f = plt.figure()
     f.set_figwidth(10)
     f.set_figheight(2)
     df =pd.DataFrame()
     skewness_list = []
     for key,value in dic.items():

          dataset = proc.get_Dataset(key)
          X_values = np.array(range(len(dataset)))
          for chunk in dataset:
               skewness = stats.skew(chunk[value])
               skewness_list.append(skewness)
          df[key] = pd.Series(skewness_list)
          skewness_list =[]

          if smooth:
               y_values = savgol_filter(df[key], 51, 1)
          else:
               y_values = df[key]

          plt.plot(range(len(df)), y_values, label = key)
     plt.rc ('axes', titlesize = 15,labelsize = 15) 
     plt.title('Skewness of all chunks from different classes')
     plt.legend(fontsize = 'small',loc = 4)
     plt.rc ('xtick', labelsize = 15)
     plt.rc ('ytick', labelsize = 15)
     plt.xlabel('chunks')
     plt.ylabel('skew')
     plt.grid(True)
     plt.show()

def show_variance_of_matfiles(dic :dict,smooth: bool):
     f = plt.figure()
     f.set_figwidth(10)
     f.set_figheight(2)
     df =pd.DataFrame()
     var_list = []
     for key,value in dic.items():

          dataset = proc.get_Dataset(key)
          X_values = np.array(range(len(dataset)))
          for chunk in dataset:
               var = np.var(chunk[value])
               var_list.append(var)
          df[key] = pd.Series(var_list)
          var_list =[]

          if smooth:
               y_values = savgol_filter(df[key], 51, 1)
          else:
               y_values = df[key]

          plt.plot(range(len(df)), y_values, label = key)
     plt.rc ('axes', titlesize = 15,labelsize = 15)  
     plt.title('Variance of all chunks from different classes')
     plt.legend(fontsize = 'small',loc = 4)
     plt.rc ('xtick', labelsize = 15)
     plt.rc ('ytick', labelsize = 15)
     plt.xlabel('chunks')
     plt.ylabel('var')
     plt.grid(True)
     plt.show()